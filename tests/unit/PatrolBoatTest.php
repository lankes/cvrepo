<?php
use Classes\PatrolBoat;

/**
 *
 */
class PatrolBoatTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     *
     */
    private $patrolBoat;

    /**
     *
     */
    protected function _before()
    {
        $this->patrolBoat = new PatrolBoat();
    }

    /**
     *
     */
    protected function _after()
    {
    }

    // tests
    /**
     * Test the returned name of the getName method is PatrolBoat
     */
    public function testGetName()
    {
        $name = $this->patrolBoat->getName();
        $this->assertEqual($name, 'PatrolBoat', 'The name is not equal PatrolBoat');

    }

    /**
     * Test the returned name of the getName method is 2
     */
    public function testGetSpaces()
    {
        $name = $this->patrolBoat->getSpaces();
        $this->assertEqual($name, '2', 'The spaces is not equal 2');

    }
}