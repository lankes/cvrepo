<?php
use Classes\AircraftCarrier;

/**
 *
 */
class AircraftCarrierTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     *
     */
    private $aircraftCarrier;

    /**
     *
     */
    protected function _before()
    {
        $this->aircraftCarrier = new AircraftCarrier();
    }

    /**
     *
     */
    protected function _after()
    {
    }

    // tests
    /**
     * Test the returned name of the getName method is AircraftCarrier
     */
    public function testGetName()
    {
        $name = $this->aircraftCarrier->getName();
        $this->assertEqual($name, 'AircraftCarrier', 'The name is not equal AircraftCarrier');

    }

    /**
     * Test the returned name of the getName method is 5
     */
    public function testGetSpaces()
    {
        $name = $this->aircraftCarrier->getSpaces();
        $this->assertEqual($name, '5', 'The spaces is not equal 5');

    }
}