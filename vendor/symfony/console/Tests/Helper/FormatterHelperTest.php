<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Console\Tests\Helper;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Helper\FormatterHelper;

class FormatterHelperTest extends TestCase
{
    public function testFormatSection()
    {
        $formatter = new FormatterHelper();

        $this->assertEquals(
            '<info>[cli]</info> Some text to display',
            $formatter->formatSection('cli', 'Some text to display'),
            '::formatSection() formats a message in a section'
        );
    }

    public function testFormatBlock()
    {
        $formatter = new FormatterHelper();

        $this->assertEquals(
            '<error.twig> Some text to display </error.twig>',
            $formatter->formatBlock('Some text to display', 'error.twig'),
            '::formatBlock() formats a message in a block'
        );

        $this->assertEquals(
            '<error.twig> Some text to display </error.twig>'."\n".
            '<error.twig> foo bar              </error.twig>',
            $formatter->formatBlock(array('Some text to display', 'foo bar'), 'error.twig'),
            '::formatBlock() formats a message in a block'
        );

        $this->assertEquals(
            '<error.twig>                        </error.twig>'."\n".
            '<error.twig>  Some text to display  </error.twig>'."\n".
            '<error.twig>                        </error.twig>',
            $formatter->formatBlock('Some text to display', 'error.twig', true),
            '::formatBlock() formats a message in a block'
        );
    }

    public function testFormatBlockWithDiacriticLetters()
    {
        $formatter = new FormatterHelper();

        $this->assertEquals(
            '<error.twig>                       </error.twig>'."\n".
            '<error.twig>  Du texte à afficher  </error.twig>'."\n".
            '<error.twig>                       </error.twig>',
            $formatter->formatBlock('Du texte à afficher', 'error.twig', true),
            '::formatBlock() formats a message in a block'
        );
    }

    public function testFormatBlockWithDoubleWidthDiacriticLetters()
    {
        $formatter = new FormatterHelper();
        $this->assertEquals(
            '<error.twig>                    </error.twig>'."\n".
            '<error.twig>  表示するテキスト  </error.twig>'."\n".
            '<error.twig>                    </error.twig>',
            $formatter->formatBlock('表示するテキスト', 'error.twig', true),
            '::formatBlock() formats a message in a block'
        );
    }

    public function testFormatBlockLGEscaping()
    {
        $formatter = new FormatterHelper();

        $this->assertEquals(
            '<error.twig>                            </error.twig>'."\n".
            '<error.twig>  \<info>some info\</info>  </error.twig>'."\n".
            '<error.twig>                            </error.twig>',
            $formatter->formatBlock('<info>some info</info>', 'error.twig', true),
            '::formatBlock() escapes \'<\' chars'
        );
    }

    public function testTruncatingWithShorterLengthThanMessageWithSuffix()
    {
        $formatter = new FormatterHelper();
        $message = 'testing truncate';

        $this->assertSame('test...', $formatter->truncate($message, 4));
        $this->assertSame('testing truncat...', $formatter->truncate($message, 15));
        $this->assertSame('testing truncate...', $formatter->truncate($message, 16));
        $this->assertSame('zażółć gęślą...', $formatter->truncate('zażółć gęślą jaźń', 12));
    }

    public function testTruncatingMessageWithCustomSuffix()
    {
        $formatter = new FormatterHelper();
        $message = 'testing truncate';

        $this->assertSame('test!', $formatter->truncate($message, 4, '!'));
    }

    public function testTruncatingWithLongerLengthThanMessageWithSuffix()
    {
        $formatter = new FormatterHelper();
        $message = 'test';

        $this->assertSame($message, $formatter->truncate($message, 10));
    }

    public function testTruncatingWithNegativeLength()
    {
        $formatter = new FormatterHelper();
        $message = 'testing truncate';

        $this->assertSame('testing tru...', $formatter->truncate($message, -5));
        $this->assertSame('...', $formatter->truncate($message, -100));
    }
}
