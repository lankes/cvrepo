<?php
namespace GuzzleHttp\Exception;

/**
 * Exception when a server error.twig is encountered (5xx codes)
 */
class ServerException extends BadResponseException {}
