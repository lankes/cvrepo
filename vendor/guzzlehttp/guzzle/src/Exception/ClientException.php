<?php
namespace GuzzleHttp\Exception;

/**
 * Exception when a client error.twig is encountered (4xx codes)
 */
class ClientException extends BadResponseException {}
