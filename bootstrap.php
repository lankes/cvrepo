<?php
use \Services;
use \Monolog;
use Classes\Grid;

require 'vendor/autoload.php';

$config = require'app/config/dev.php';

$app = new \Slim\App($config);

$container = $app->getContainer();

// Validate that the config settings are available
if (false === $container->offsetGet('settings')) {
    throw new \InvalidArgumentException('Configuration not found !');
}

// setup methods for container
setUpServices($container);
setUpLogging($container);
setUpTemplates($container);
setUpValueObjectFactories($container);
setUpVessels($container);
setUpGrid($container);
setUpStorage($container);

//include the routes for the application
require_once 'app/routes/routes.php';
$app->run();

/**
 * setUpServices
 * Mount every file in app/sevices as a service
 */
function setUpServices($container) {
    foreach ( scandir(APP_PATH . "app/sevices") as $file) {
        if (pathinfo($file, PATHINFO_EXTENSION) === "php") {
            $bits = explode(".", $file);
            $class = $bits[0];
            $service = "Services\\" . $class;

            // mount the services - this imports the routes defined in each
            $container[lcfirst($class)] = function ($service) {
                $myService = new $service();
                return $myService;
            };
        }
    }
}

/**
 * Set up logger in container
 */
function setUpLogging($container)
{
    $container['logger'] = function ($c) {
        $settings = $c->get('settings')['logger'];
        $logger = new Monolog\Logger($settings['name']);
        $logger->pushProcessor(new Monolog\Processor\UidProcessor());
        $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
        return $logger;
    };
}

function setUpGrid($container)
{
    $container['grid'] = function ($c) {
        $gridSettings = $c->get('settings')['grid'];
        $grid = new Grid();
        $grid->setX($gridSettings['x']);
        $grid->setY($gridSettings['y']);
        return $grid;
    };
}

/**
 * set up template engine within container
 */
function setUpTemplates($container)
{
    $container['view'] = function ($container) {
        $templates = '../app/templates/';
        $cache = '../app/cache/templates/';

        $view = new Slim\Views\Twig($templates, compact('cache'));

        return $view;
    };
}

/**
 * Set up of Value Object factories within the container
 */
function setUpValueObjectFactories($container)
{
    $factories = ['VesselFactory', 'StorageFactory'];
    foreach ( $factories as $factory) {
        // mount the services - this imports the routes defined in each
        $container[lcfirst($factory)] = function ($factory) {
            $containerFactory = new $factory();
            return $containerFactory;
        };
    }
}

/**
 * Factory to add vessel types to container
 */
function setUpVessels($container)
{
    $container['vessels'] = function ($container) {
        $vesselTypes = $container->get('settings')['vessels'];
        foreach ($vesselTypes as $vesselType) {
            $vessels[$vesselType] = $container['vesselFactory']->create($vesselType);
        }
        return $vessels;
    };
}

/**
 * Factory to add storage to container
 */
function setUpStorage($container)
{
    $container['storage'] = function ($container) {
        $storageType = $container->get('settings')['storage'];
        $storage = $container['storageFactory']->create($storageType);
        return $storage;
    };
}
