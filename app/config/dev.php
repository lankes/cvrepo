<?php
return [
    'settings' => [
        'displayErrorDetails' => true,
        'vessels' => [
            'AircraftCarrier',
            'Battleship',
            'Destroyer',
            'Submarine',
            'PatrolBoat'
        ],
        'grid' => [
            'x' => 10,
            'y' => 10
        ],
        'storage' => [
            'Session'
        ],
        'logger' => [
            'name' => 'battleship',
            'level' => Monolog\Logger::DEBUG,
            'path' => __DIR__ . '/../logs/global-' . date("Y-m-d") . '.log',
        ],
    ]
];
