<?php
return [
    'settings' => [
        'displayErrorDetails' => true,
        'vessels' => [
            'AircraftCarrier',
            'Battleship',
            'Destroyer',
            'Submarine',
            'PatrolBoat'
        ],
        'grid' => [
            'x' => 10,
            'y' => 10
        ],
        'storage' => [
            'session'
        ],
        'logging' => true
    ]
];