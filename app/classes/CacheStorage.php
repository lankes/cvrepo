<?php
namespace Classes;

/**
 * Cache implementation of the Storage interface
 */
class CacheStorage implements Storage
{
    /**
     * Gets the stored values from the Cache
     */
    public function getStored()
    {
        //@todo
    }

    /**
     * Updates the stored values in the Cache
     */
    public function updateStored()
    {
        //@odo
    }

    /**
     * Creates the stored values in the Cache
     */
    public function createStored()
    {
        //@todo
    }

    /**
     * Deletes the stored values from the Cache
     */
    public function deleteStored()
    {
        //@todo
    }
}