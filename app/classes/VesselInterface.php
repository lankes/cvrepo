<?php
namespace classes;


/**
 * Interface for Vessel,
 * This provides support for Liskov substitution principle
 */
interface VesselInterface
{
    /**
     * get the name of the ship
     */
    public function getName();

    /**
     * get the number of spaces defining the Vessels size
     */
    public function getSpaces();
}