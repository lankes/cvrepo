<?php
namespace Classes;

/**
 * Session implementation of the Storage interface
 */
class SessionStorage implements Storage
{
    /**
     * Gets the stored values from the Session
     */
    public function getStored()
    {
        //@todo
    }

    /**
     * Updates the stored values in the Session
     */
    public function updateStored()
    {
        //@odo
    }

    /**
     * Creates the stored values in the Session
     */
    public function createStored()
    {
        //@todo
    }

    /**
     * Deletes the stored values from the Session
     */
    public function deleteStored()
    {
        //@todo
    }
}