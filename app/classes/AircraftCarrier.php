<?php
namespace Classes;

/**
 * AircraftCarrier type of Vessel
 * This conforms to the Interface segregation principle, implementing all methods within the Vessel interface
 */
class AircraftCarrier implements VesselInterface
{
    /**
     * name of type of vessel
     * @string
     */
    protected $name = __CLASS__;

    /**
     * number of spaces of vessel size
     * @int
     */
    protected $spaces = 5;

    /**
     * Get name of AircraftCarrier
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Number of spaces for vessel
     * @return int
     */
    public function getSpaces()
    {
        return $this->spaces;
    }
}