<?php
namespace classes;

/**
 * Grid layout for game play
 * Concise classes help support Single responsibility principle
 */
class Grid
{
    /**
     * maximum X coordinate of the Board Grid
     */
    private $x = 0;

    /**
     * maximum Y coordinate of the Board Grid
     */
    private $y = 0;

    /**
     * set X coordinate of the Board Grid
     * @param int $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * set Y coordinate of the Board Grid
     * @param int $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }

    /**
     * get X coordinate of the Board Grid
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * get Y coordinate of the Board Grid
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }
}