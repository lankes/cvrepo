<?php
namespace Classes;

interface Storage
{
    public function getStored();
    public function updateStored();
    public function createStored();
    public function deleteStored();
}