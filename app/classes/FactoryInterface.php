<?php
namespace Classes;

/**
 * This provides support for Liskov substitution principle
 */
interface FactoryInterface
{
    /**
     * creates objects of given type
     * @param VesselInterface $type
     */
    public function create($type);
}