<?php
namespace Classes;

use Vessel;

/**
 * Vessel factory
 * This provides support for Liskov substitution principle
 * Also this class supports Single responsibility principle
 */
class VesselFactory Implements FactoryInterface
{
    /**
     * Generate vessel of type passed
     * @param VesselInterface $type
     * @return VesselInterface
     * @throws Exception
     */
    public function create($type)
    {
        if($type instanceof VesselInterface) {
            if (!class_exists($type, false /* do not attempt autoload */)) {
                throw new Exception("Unknown class $type");
            }
            return new $type();
        } else {
            throw new Exception("Is not of instanceof VesselInterface");
        }
    }
}