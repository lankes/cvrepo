<?php
namespace Classes;

/**
 * File implementation of the Storage interface
 */
class FileStorage implements Storage
{
    /**
     * Gets the stored values from the File
     */
    public function getStored()
    {
        //@todo
    }

    /**
     * Updates the stored values in the File
     */
    public function updateStored()
    {
        //@odo
    }

    /**
     * Creates the stored values in the File
     */
    public function createStored()
    {
        //@todo
    }

    /**
     * Deletes the stored values from the File
     */
    public function deleteStored()
    {
        //@todo
    }
}