<?php
namespace classes;

/**
 * Submarine type of Vessel
 * This conforms to the Interface segregation principle, implementing all methods within the Vessel interface
 */
class Submarine implements VesselInterface
{
    /**
     * name of type of vessel
     * @string
     */
    protected $name = __CLASS__;

    /**
     * number of spaces of vessel size
     * @int
     */
    protected $spaces = 3;

    /**
     * Get name of Submarine
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Number of spaces for vessel
     * @return int
     */
    public function getSpaces()
    {
        return $this->spaces;
    }
}