<?php
namespace Classes;

use Storage;

/**
 * Storage factory
 * This provides support for Liskov substitution principle
 * Also this class supports Single responsibility principle
 */
class StorageFactory implements FactoryInterface
{
    /**
     * Generate Storage of type passed
     * @param Storage $type
     * @return Storage
     * @throws Exception
     */
    public function create($type)
    {
        if($type instanceof StorageInterface) {
            if (!class_exists($type, false /* do not attempt autoload */)) {
                throw new Exception("Unknown class $type");
            }
            return new $type();
        } else {
            throw new Exception("Is not of instanceof StorageInterface");
        }
    }
}