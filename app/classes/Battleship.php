<?php
namespace classes;

/**
 * Battleship type of Vessel
 * This conforms to the Interface segregation principle, implementing all methods within the Vessel interface
 */
class Battleship implements VesselInterface
{
    /**
     * name of type of vessel
     * @string
     */
    protected $name = __CLASS__;
    /**
     * number of spaces of vessel size
     * @int
     */
    protected $spaces = 4;

    /**
     * Get name of Battleship
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Size of the Battleship
     * @return int
     */
    public function getSpaces()
    {
        return $this->spaces;
    }
}