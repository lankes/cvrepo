<?php
$app->group('/battleship', function () {

    $this->group('/', function () {
        $this->get('', 'Controllers\GamePlayController:getGamePlay');
        $this->post('', 'Controllers\GamePlayController:createGamePlay');
    });

    $this->group('/api/v1', function () {

        $this->group('/gameplay', function () {
            $this->get('', 'Controllers\GamePlayApiController:getGamePlays');
            $this->post('', 'Controllers\GamePlayApiController:createGamePlay');
            $this->delete('', 'Controllers\GamePlayApiController:deleteGamePlay');

            $this->group('/{coordinate}', function () {
                $this->get('', 'Controllers\GamePlayApiController:getGamePlay');
                $this->put('', 'Controllers\GamePlayApiController:updateGamePlay');
            });
        });
    });
});
