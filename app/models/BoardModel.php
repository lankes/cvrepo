<?php
namespace Models;

/**
 * Board Model
 * Aggregate of the required elements
 */
class BoardModel
{
    /**
     * board property
     */
    private $board = [];

    /**
     * get the boar
     * aggregate of the vessel and grid value objects
     */
    public function getBoard()
    {
        $this->board['vessels'] = $this->getVessels();
        $this->board['grid'] = $this->getGrid();
        return $this->board;
    }

    /**
     * get the vessel
     */
    private function getVessels()
    {
        $vesselTypes = $container->get('settings')['vessels'];
        foreach($vesselTypes as $vesselType) {
            $vessels[$vesselType] = $container['vesselFactory']->create($vesselType);
        }
        return $vessels;
    }

    /**
     * get the grid
     */
    private function getGrid()
    {
        $grid = $container->get('settings')['grid'];
        return $grid;
    }
}