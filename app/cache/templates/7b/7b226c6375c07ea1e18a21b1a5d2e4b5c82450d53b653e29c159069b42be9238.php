<?php

/* boardLayout.twig */
class __TwigTemplate_a78940655f8399010c898e7c3fbd6449f23b3445a1a05eef53d9d0bc1d32fff8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"POST\">
    ";
        // line 2
        echo twig_include($this->env, $context, "boardComponent.twig");
        echo "
    <div class=\"col-sm-6\">
        <input type=\"submit\" value=\"submit\" name=\"submit\">
    </div>
    <div class=\"col-sm-6\">
        <input type=\"submit\" value=\"reset\" name=\"reset\">
    </div>
</form>";
    }

    public function getTemplateName()
    {
        return "boardLayout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "boardLayout.twig", "/home/future/Projects/phpBattleship/app/templates/boardLayout.twig");
    }
}
