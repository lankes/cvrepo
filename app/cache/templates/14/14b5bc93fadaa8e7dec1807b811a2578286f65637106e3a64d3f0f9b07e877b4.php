<?php

/* main.twig */
class __TwigTemplate_62b2a7f6fd0cc48e644034402aa8dd8481939e89928648327d1df6bcd60210b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"
        \"http://www.w3.org/TR/html4/loose.dtd\">
<html>
<head>
    ";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 8
        echo "</head>
<body>
<div id=\"content\" class=\"container-fluid\">";
        // line 10
        $this->displayBlock('content', $context, $blocks);
        echo "</div>
<div id=\"footer\">
    ";
        // line 12
        $this->displayBlock('footer', $context, $blocks);
        // line 15
        echo "</div>
</body>
</html>";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "        <title>";
        $this->displayBlock('title', $context, $blocks);
        echo " - Battleship</title>
    ";
    }

    public function block_title($context, array $blocks = array())
    {
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
    }

    // line 12
    public function block_footer($context, array $blocks = array())
    {
        // line 13
        echo "        &copy; Copyright 2017 by Battleship.
    ";
    }

    public function getTemplateName()
    {
        return "main.twig";
    }

    public function getDebugInfo()
    {
        return array (  70 => 13,  67 => 12,  62 => 10,  51 => 6,  48 => 5,  42 => 15,  40 => 12,  35 => 10,  31 => 8,  29 => 5,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "main.twig", "/home/future/Projects/phpBattleship/app/templates/main.twig");
    }
}
