<?php

/* gameLayout.twig */
class __TwigTemplate_7d519863b1e9488dba217f524a63ee313a9ec1d30074f82d24c705f05f970ef1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("main.twig", "gameLayout.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "main.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Game Layout";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "    <!-- Latest compiled and minified CSS -->
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
";
    }

    // line 8
    public function block_content($context, array $blocks = array())
    {
        // line 9
        echo "    <h2>Battleships</h2>
    ";
        // line 10
        if ((twig_length_filter($this->env, ($context["moves"] ?? null)) > 0)) {
            // line 11
            echo "        ";
            echo twig_include($this->env, $context, "boardLayout.twig");
            echo "
    ";
        } else {
            // line 13
            echo "        ";
            echo twig_include($this->env, $context, "errorPartial.twig");
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "gameLayout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 13,  53 => 11,  51 => 10,  48 => 9,  45 => 8,  39 => 5,  36 => 4,  30 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "gameLayout.twig", "/home/future/Projects/phpBattleship/app/templates/gameLayout.twig");
    }
}
