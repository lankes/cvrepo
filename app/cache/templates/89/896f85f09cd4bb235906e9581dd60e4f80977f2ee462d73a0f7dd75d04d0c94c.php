<?php

/* boardCell.twig */
class __TwigTemplate_13ed390624a7e16e73c44f7811036c06cfc73b9ae1751812af73a1900c010773 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"col-sm-1\">
    ";
        // line 2
        $context["disabled"] = "";
        // line 3
        echo "    ";
        if ((twig_length_filter($this->env, ($context["moves"] ?? null)) > 40)) {
            // line 4
            echo "        ";
            $context["disabled"] = "disabled='disabled'";
            // line 5
            echo "    ";
        }
        // line 6
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["moves"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 7
            echo "        ";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo " : ";
            echo twig_escape_filter($this->env, ($context["coordinate"] ?? null), "html", null, true);
            echo "
        : ";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["moves"] ?? null), "coordinate", array()), "html", null, true);
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), ($context["moves"] ?? null), "coordinate", array(), "any", true, true)) {
            // line 11
            echo "        ";
            if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["moves"] ?? null), "coordinate", array()) == "hit")) {
                // line 12
                echo "            'H'
        ";
            } elseif ((twig_get_attribute($this->env, $this->getSourceContext(),             // line 13
($context["moves"] ?? null), "coordinate", array()) === "miss")) {
                // line 14
                echo "           'M';
        ";
            }
            // line 16
            echo "    ";
        } else {
            // line 17
            echo "        <input type='radio' id='board_";
            echo twig_escape_filter($this->env, ($context["coordinate"] ?? null), "html", null, true);
            echo "' value='";
            echo twig_escape_filter($this->env, ($context["coordinate"] ?? null), "html", null, true);
            echo "' name='board' ";
            echo twig_escape_filter($this->env, ($context["disabled"] ?? null), "html", null, true);
            echo ">
    ";
        }
        // line 19
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "boardCell.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 19,  73 => 17,  70 => 16,  66 => 14,  64 => 13,  61 => 12,  58 => 11,  55 => 10,  47 => 8,  38 => 7,  33 => 6,  30 => 5,  27 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "boardCell.twig", "/home/future/Projects/phpBattleship/app/templates/boardCell.twig");
    }
}
