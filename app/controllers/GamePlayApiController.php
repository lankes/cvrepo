<?php
namespace Controllers;

/**
 * Game play controller for restful Api
 */
class GamePlayApiController
{
    /**
     * Setups the board and moves
     */
    public function getGamePlays()
    {
        try {

            $boardBuildService = $this->checkServiceAvailablity('boardBuildService');
            $gamePlayService = $this->checkServiceAvailablity('gamePlayService');

            $board = $boardBuildService->getBoard();
            $moves = $gamePlayService->getMoves();

            $result = [
                "status" => "success",
                "data" => [
                    "board" => $board,
                    "moves" => $moves],
                "message" => null
            ];
            return $response->withStatus(200)->getBody()->write($result->toJson());
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            $this->logger->addInfo($msg);
            $result = [
                "status" => "error.twig",
                "data" => [],
                "message" => "There was an issue with request"
            ];
            return $response->withStatus(404)->getBody()->write($result->toJson());
        }

    }

    /**
     * adds new move to the game
     */
    public function createGamePlay()
    {
        // @todo
    }

    /**
     * resets the game
     */
    public function deleteGamePlay()
    {
        // @todo
    }

    /**
     * get individual
     */
    public function getGamePlay()
    {
        // @todo
    }

    /**
     * update individual move for game
     *
     */
    public function updateGamePlay()
    {
        // @todo
    }

    /**
     *
     */
    private function checkServiceAvailablity($type)
    {
        if($this->has($type)) {
            return $service = $this->$type;
        } else {
            throw new \Exception("$type Service is not available");
        }
    }
}