<?php
namespace Controllers;

/**
 * To display the game via template
 */
class GamePlayController
{
    /**
     * Get the board and moves made player
     */
    public function getGamePlay()
    {
        try {

            $boardBuildService = $this->checkServiceAvailablity('boardBuildService');
            $gamePlayService = $this->checkServiceAvailablity('gamePlayService');

            $board = $boardBuildService->getBoard();
            $moves = $gamePlayService->getMoves();
            return $this->view->render($response, 'gameLayout.twig', ['moves' => $moves, 'board' => $board]);
        } catch(Exception $e) {
            $msg = $e->getMessage();
            $this->logger->addInfo($msg);
            return $this->view->render($response, 'error.twig',[]);
        }


    }

    /**
     * Initialise board
     */
    public function createGamePlay()
    {
        // @todo
    }

    /**
     * Reset the board and Game play
     */
    public function deleteGamePlay()
    {
        // @todo
    }

    /**
     * Update individual moves within game play
     */
    public function updateGamePlay()
    {
        // @todo
    }

    /**
     * Check if the service is available
     */
    private function checkServiceAvailablity($type)
    {
        if($this->has($type)) {
            return $service = $this->$type;
        } else {
            throw new \Exception("$type Service is not available");
        }
    }
}